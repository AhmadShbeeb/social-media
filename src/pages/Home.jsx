import { Button, Center, Group, Loader, Modal, SimpleGrid, Stack, TextInput, Title } from '@mantine/core'
import { useForm } from '@mantine/form'
import { useMutation, useQuery, useQueryClient } from '@tanstack/react-query'
import axios from 'axios'
import { useEffect, useState } from 'react'

import { Post } from '~/components'

export const Home = () => {
  const [loggedUser, setLoggedUser] = useState({})
  const [opened, setOpened] = useState(false)

  const [favShowed, setFavShowed] = useState(0)

  const { data: userPosts, isLoading } = useQuery(['posts', loggedUser?.email], async ({ signal }) => {
    const { data } = await axios.get(`http://localhost:3000/posts`)
    // console.log({ loggedUser, data })
    return data
  })

  const queryClient = useQueryClient()

  useEffect(() => {
    setLoggedUser(JSON.parse(localStorage.getItem('login')))
  }, [userPosts])

  const handleCreate = () => {
    setOpened(1)
  }

  const form = useForm({
    initialValues: { title: '', body: '' },
  })

  const { data: post, mutate } = useMutation(
    async values => {
      const { data } = await axios.post(`http://localhost:3000/posts`, {
        title: values?.title,
        body: values?.body,
        owner: loggedUser?.email,
        likedBy: [],
      })
      return data
    },
    {
      onSuccess: res => {
        setOpened(0)
        queryClient.invalidateQueries(['posts', loggedUser?.email])
      },
    }
  )

  const { data: favPosts, mutate: showFav } = useMutation(
    async values => {
      const { data } = await axios.get(`http://localhost:3000/favourite?user_like=${loggedUser?.email}`)

      const favPo = data?.map(f => f?.postId)

      const filltered = userPosts?.filter(p => {
        for (let i = 0; i < favPo.length; i++) {
          if (p?.id === favPo[i]) return true
        }
        return false
      })
      return filltered
    },
    {
      onSuccess: res => {
        queryClient.setQueryData(['posts', loggedUser?.email], res)
        setFavShowed(1)
      },
    }
  )

  const handleShowFav = () => {
    showFav()
  }

  const handleShowAll = () => {
    queryClient.invalidateQueries(['posts', loggedUser?.email])
    setFavShowed(0)
  }

  if (isLoading) return <Loader variant='bars' />
  return (
    <>
      <Modal opened={opened} onClose={() => setOpened(false)} title='Create New Post!'>
        <form id='steps-form' onSubmit={form.onSubmit(values => mutate(values))}>
          <Stack>
            <TextInput required label='Title' {...form.getInputProps('title')} />
            <TextInput required label='Body' {...form.getInputProps('body')} />

            <Group position='center' m='sm' p='sm'>
              <Button type='submit'>Create</Button>
            </Group>
          </Stack>
        </form>
      </Modal>

      <Button m={10} color='red' onClick={handleShowFav}>
        Show Favourite
      </Button>
      <Button m={10} color='blue' onClick={handleShowAll}>
        Show All
      </Button>

      <Title size='h3' color='red' sx={{ textAlign: 'center' }}>
        Welcome {loggedUser?.email}
      </Title>
      <SimpleGrid cols={3} mt={30}>
        {userPosts ? (
          userPosts.map(p => <Post key={p?.id} post={p} loggedUser={loggedUser} favShowed={favShowed} />)
        ) : (
          <h1>No posts yet</h1>
        )}
      </SimpleGrid>
      <Center>
        <Button m={10} color='red' onClick={handleCreate}>
          Create New Post
        </Button>
      </Center>
    </>
  )
}
