import { Button, Center, Group, Paper, PasswordInput, Stack, TextInput } from '@mantine/core'
import { useForm } from '@mantine/form'
import { IconLock, IconMail } from '@tabler/icons'
import { useMutation } from '@tanstack/react-query'
import axios from 'axios'
import { useOutletContext, useNavigate } from 'react-router-dom'

export const Login = () => {
  const navigate = useNavigate()

  const { setIsLogged } = useOutletContext()

  const iconSize = 20
  const iconStroke = 2

  const form = useForm({
    initialValues: { email: '', password: '' },

    validate: {
      email: value => (/^\S+@\S+$/.test(value) ? null : 'Invalid email'),
    },
  })

  const { data: user, mutate } = useMutation(
    async values => {
      const { data: foundUser } = await axios.get(
        `http://localhost:3000/users?email=${values.email.toLocaleLowerCase()}`
      )
      // console.log(foundUser)
      if (foundUser?.length > 0) {
        if (foundUser[0]?.password !== values?.password) return { message: 'wrong password' }
        return foundUser
      }
      return { message: 'user not found' }
    },
    {
      onSuccess: res => {
        if (res?.message) {
          alert(res.message)
          return
        }
        localStorage.setItem('login', JSON.stringify(res[0]))
        setIsLogged(1)
        navigate('/')
      },
    }
  )

  return (
    <Center sx={{ height: '85vh' }}>
      <form id='steps-form' onSubmit={form.onSubmit(values => mutate(values))}>
        <Paper shadow='xs' m='lg' p='lg' radius='lg' mt={110} px={30} py={30}>
          <Stack>
            <TextInput
              required
              label='Email address'
              icon={<IconMail size={iconSize} stroke={iconStroke} />}
              {...form.getInputProps('email')}
            />
            <PasswordInput
              label='Password'
              required
              icon={<IconLock size={iconSize} stroke={iconStroke} />}
              {...form.getInputProps('password')}
            />

            <Group position='center' m='sm' p='sm'>
              <Button type='submit'>Login</Button>
            </Group>
          </Stack>
        </Paper>
      </form>
    </Center>
  )
}
