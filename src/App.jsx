import { BrowserRouter, Route, Routes } from 'react-router-dom'
import { Layout } from './components'
import { Home, Login } from './pages'

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path='/' exact element={<Layout />}>
          <Route index element={<Home />} />
          <Route path='/login' element={<Login />} />
        </Route>
      </Routes>
    </BrowserRouter>
  )
}

export default App
