import { Badge, Button, Card, Group, Image, Text } from '@mantine/core'
import { IconHeart, IconThumbUp } from '@tabler/icons'

import { useMutation, useQueryClient } from '@tanstack/react-query'
import axios from 'axios'

export const Post = propData => {
  const iconSize = 20
  const iconStroke = 2

  const queryClient = useQueryClient()

  const { data: likedPost, mutate: updatePost } = useMutation(
    async values => {
      const { data } = await axios.put(`http://localhost:3000/posts/${propData?.post?.id}`, {
        // ...propData?.post,
        id: propData?.post?.id,
        title: propData?.post?.title,
        body: propData?.post?.body,
        owner: propData?.post?.owner,
        likedBy: [...propData?.post?.likedBy, propData?.loggedUser?.email],
      })
      return data
    },
    {
      onSuccess: async res => {
        if (propData?.favShowed) {
          const { data: favPosts } = await axios.get(
            `http://localhost:3000/favourite?user_like=${propData?.loggedUser?.email}`
          )
          const { data: userPosts } = await axios.get(`http://localhost:3000/posts`)

          console.log({ favPosts, userPosts })
          const favPo = favPosts?.map(f => f?.postId)

          const filltered = userPosts?.filter(p => {
            for (let i = 0; i < favPo.length; i++) {
              if (p?.id === favPo[i]) return true
            }
            return false
          })
          queryClient.setQueryData(['posts', propData?.loggedUser?.email], filltered)
        } else {
          queryClient.invalidateQueries(['posts', propData?.loggedUser?.email])
        }
      },
    }
  )

  const { data: unlikedPost, mutate: removeLike } = useMutation(
    async values => {
      const { data } = await axios.put(`http://localhost:3000/posts/${propData?.post?.id}`, {
        id: propData?.post?.id,
        title: propData?.post?.title,
        body: propData?.post?.body,
        owner: propData?.post?.owner,
        likedBy: values,
      })
      return data
    },
    {
      onSuccess: async res => {
        if (propData?.favShowed) {
          const { data: favPosts } = await axios.get(
            `http://localhost:3000/favourite?user_like=${propData?.loggedUser?.email}`
          )
          const { data: userPosts } = await axios.get(`http://localhost:3000/posts`)

          console.log({ favPosts, userPosts })
          const favPo = favPosts?.map(f => f?.postId)

          const filltered = userPosts?.filter(p => {
            for (let i = 0; i < favPo.length; i++) {
              if (p?.id === favPo[i]) return true
            }
            return false
          })
          queryClient.setQueryData(['posts', propData?.loggedUser?.email], filltered)
        } else {
          queryClient.invalidateQueries(['posts', propData?.loggedUser?.email])
        }
      },
    }
  )

  const { data: favouritePost, mutate: addToFav } = useMutation(
    async values => {
      const { data: allFav } = await axios.get(`http://localhost:3000/favourite/`)
      console.log(allFav)
      const isFaved = allFav?.find(f => f?.user === propData?.loggedUser?.email && f?.post?.id === propData?.post?.id)
      if (isFaved) {
        alert('already added')
      } else {
        alert('added to favourite')
        const { data } = await axios.post(`http://localhost:3000/favourite/`, {
          user: propData?.loggedUser?.email,
          postId: propData?.post?.id,
        })
        return data
      }
    },
    {
      onSuccess: res => {
        queryClient.invalidateQueries(['posts', propData?.loggedUser?.email])
      },
    }
  )

  const handleLike = () => {
    const alreadyLiked = propData?.post?.likedBy?.find(liked => liked === propData?.loggedUser?.email)
    if (alreadyLiked) {
      const res = propData?.post?.likedBy?.filter(liked => liked !== propData?.loggedUser?.email)
      removeLike(res)
    } else {
      updatePost()
    }
  }

  const handleFav = () => {
    addToFav()
  }

  return (
    <Card shadow='sm' p='lg' radius='md'>
      <Card.Section>
        <Image
          src='https://images.unsplash.com/photo-1527004013197-933c4bb611b3?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=720&q=80'
          height={160}
          alt='Norway'
        />
      </Card.Section>

      <Group position='apart' mt='md' mb='xs'>
        <Text weight={500}> {propData?.post?.title}</Text>
        <Badge color='pink' variant='light'>
          By: {propData?.post?.owner}
        </Badge>
      </Group>

      <Text size='sm' color='dimmed'>
        {propData?.post?.body}
      </Text>
      <Text size='xs'>Liked By {JSON.stringify(propData?.post?.likedBy)}</Text>

      <Button.Group mt={8} sx={{ justifyContent: 'center' }}>
        <Button leftIcon={<IconThumbUp size={iconSize} stroke={iconStroke} />} onClick={handleLike}>
          Like
        </Button>
        <Button color='red' leftIcon={<IconHeart size={iconSize} stroke={iconStroke} />} onClick={handleFav}>
          add to favourite
        </Button>
      </Button.Group>
    </Card>
  )
}
