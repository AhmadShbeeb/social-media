import { AppShell } from '@mantine/core'
import { useState, useEffect } from 'react'

import { useNavigate, Outlet } from 'react-router-dom'

import bg from '../assets/bg.jpg'
import { Header } from './Header'

export const Layout = () => {
  const navigate = useNavigate()
  const [isLogged, setIsLogged] = useState(1)

  useEffect(() => {
    const login = JSON.parse(localStorage.getItem('login'))
    if (!login) {
      // console.log(isLogged)
      setIsLogged(0)
      navigate('/login')
    }
  }, [navigate])

  return (
    <AppShell
      padding='md'
      header={<Header isLogged={isLogged} />}
      styles={{
        main: {
          minHeight: '100%',
          backgroundSize: 'cover',
          backgroundRepeat: 'no-repeat',
          backgroundPosition: 'center center',
          // backgroundAttachment: 'fixed',
          // height: '86vh',
          height: '100%',
          // backgroundImage: `url(${bg})`,
          backgroundColor: '#ddd',
        },
      }}>
      <Outlet
        context={{
          isLogged,
          setIsLogged,
        }}
      />
    </AppShell>
  )
}
